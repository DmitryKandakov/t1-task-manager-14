package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}